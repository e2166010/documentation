---
title: "Gestion des conflits"
weight: 5
date: 2022-01-11T14:19:27Z
draft: false
---

Deux personnes, A et B, travaillent ensemble. Il ont créé un dépôt distant et ont tout les deux une copie locale de ce dépôt. Le dépôt contient un fichier doc1.txt. Ce fichier contient simplement 10 lignes numérotées. Dans les diagrammes, les commit de A apparaissent en bleu et ceux de B en orange. Voici l’état initial des trois dépôts:  
![conflit1](http://ml.cm9.ca/cours4d1/images/gitConflit1.png) 

A fait une modification à la ligne 2 du fichier et fait un commit, suivi d’un push. Ensuite, B fait lui aussi une modification à la ligne 2 du fichier et un commit.  
![conflit2](http://ml.cm9.ca/cours4d1/images/gitConflit2.png) 

B tente de faire un push, mais l’opération est refusée parce que sa branche main n’est pas à jour. Il doit premièrement faire un fetch.  Le fetch crée une branche correspondant à la branche origin/main (la branche main sur le dépôt distant contenant le dernier commit de A). 
![conflit3](http://ml.cm9.ca/cours4d1/images/gitConflit3.png) 

B doit maintenant faire un merge pour fusionner les deux branches. L'opération Merge réussit dans la plupart des cas à faire la fusion sans problème (exemple: lorsque les modifications ne sont pas dans le même fichier ou bien dans des régions différentes du même fichier). Puisqu'ici, A et B ont modifié la même région du fichier, la fusion génére un conflit. Voici l’état du fichier :  
{{% notice note doc1.txt %}}
1  
`<<<<<< HEAD`  
2  Modification B  
`=======`  
2  Modification A  
`>>>>>> main`  
3  
4  
5   
6  
7  
8  
9  
10  
{{% /notice %}}
Le fichier contient les modifications de la branche origin/main qui n'ont pas pu être fusionnées avec celles de la branche main locale. Les lignes dans la partie main contiennent les modifications dans origin/main et celles dans la partie HEAD contiennent les modifications locales. Il faut utiliser un éditeur de conflits pour régler la situation, en choisissant de prendre les modifications locales ou bien celles sur le serveur. À la fin de la gestion du conflit, le merge est complété et le dépôt de B se retrouve dans l'état suivant:  
![conflit4](http://ml.cm9.ca/cours4d1/images/gitConflit4.png) 
B peut maintenant faire un push pour mettre à jour le dépôt distant.  
**Note:** la commande Pull est équivalente à faire un Fetch suivi d'un Merge.  