---
title: "Flux de travail (Workflow)"
weight: 6
date: 2022-01-11T14:19:27Z
draft: true
---

Pour travailler efficacement avec Git, il est important d'avoir une stratégie d'utilisation des branches. Cette stratégie permet de:  
* s'isoler des autres développeurs (en créant une branche) pour ne pas perturber leur travail;
* récupérer le travail des autres (en fusionnant leur branche à la nôtre);
* conserver un historique clair pour être capable de revenir facilement à un état particulier du code (une version de logiciel, une livraison, la correction d'un bug, etc).  

Cette stratégie d'utilisation de Git s'appelle un flux de travail (workflow). Un workflow en Git est essentiellement une manière particulière d'utiliser les branches pour faire évoluer le code pendant toute la vie de l'application en définissant comment se feront:  
* le développement d'une fonctionnalité;
* la résolution d'un bug;
* la gestion des livraisons.    

Au fil du temps, plusieurs workflows ont été établis. Chacun offre des avantages selon le type et la taille du projet. 
Pour une description de quelques workflows utilisés, consultez la documentation de [Bitbucket](https://www.atlassian.com/git/tutorials/comparing-workflows).  
Pour ce projet, nous utiliserons le flux de travail ["Gitflow"](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).  
## La branche main
Cette branche est utilisée seulement pour conserver l'historique des livraisons. Il n'y a pas de développement qui se fait sur cette branche. Le code est développé sur d'autres branches pour être fusionné éventuellement à la branche main. En examinant les fusions vers cette branche, on peut voir la livraison des nouvelles versions ainsi que les correctifs (hotfixs) effectués à la version en production.  
{{% notice info Note %}}
Le code en production correspond au code qui est exécuté chez le client, ou mis en service. Lorsqu'un bug est détecté et que sa correction ne peut attendre à la prochaine livraison, on produit un correctif (hotfix) pour le client.  
{{% /notice %}} 
## La branche dev
C'est sur cette branche que se fait tout le développement du code. Plus précisément, la branche dev sert à intégrer le code des fonctionnalités qui sont développées sur leur branche respective. De la même façon que la branche main contient l'historique des livraisons, la branche dev contient l'historique du développement du code.
## Les branches de fonctionnalité
Chaque nouvelle fonctionnalité est développée sur sa propre branche. Ceci permet de développer plusieurs fonctionnalités en même temps sans interférence entre les équipes de développement. Lorsque le développement d'une fonctionnalité commence, on crée une branche à partir de la branche dev. Lorsqu'elle est terminée, on fusionne la branche de la fonctionnalité à la branche dev.  
Dans l'illustration plus bas, deux branches de fonctionnalités sont créées poour ensuites être fusionnées à la branche dev.   

![gitflow1](http://ml.cm9.ca/cours4d1/images/gitflow1.png) 
## Fusion d'une fonctionnalité à la branche dev
Lorsque vient le temps de fusionner un branche de fonctionnalité à la branche dev, il faut habituellement procéder en deux temps. Ici, L'équipe qui travaille sur la fonctionnalité 1 a terminé le développement et a fusionné avec la branche dev. Pendant ce temps, le développement de la fonctionnalité 2 se poursuit.  
![gitflow2](http://ml.cm9.ca/cours4d1/images/gitflow2.png) 
Lorsque l'équipe termine la fonctionnalité 2, elle doit la fusionner avec la branche dev. Mais la branche dev a évolué depuis la création de la branche. Le code dans dev n'est plus dans le même état que lorsque le développement de la fonctionnalité a commencé. Avant de faire la fusion de fonct 2 avec dev, il faut fusionner la branche dev avec fonct2 et s'assurer que le nouveau code sur dev n'a pas impacté la nouvelle fonctionnalité. Après la fusion, l'équipe doit refaire les tests sur sa fonctionnalité ainsi que ceux de l'application.  

![gitflow3](http://ml.cm9.ca/cours4d1/images/gitflow3.png) 
Lorsque tout fonctionne correctement, la branche fonctionnalité 2 peut être fusionnée avec dev.  

![gitflow4](http://ml.cm9.ca/cours4d1/images/gitflow4.png) 
